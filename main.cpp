/*
fe hacker 2017

compile and execute:
g++ main.cpp -o vpc && ./vpc disk.img (you need to get the 'disk.img')
*/

#include <assert.h>
#include <fcntl.h>

#include <iostream>
 using namespace std;

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
 
using namespace std;

#define MEM_SIZE 0x100000
#define SECTOR_SIZE 0x200

#define REG_Z 0
#define REG_PC 63

enum {
  OP_LOAD_B = 0,
  OP_LOAD_H = 1,
  OP_LOAD_W = 2,
  OP_STORE_B = 4,
  OP_STORE_H = 5,
  OP_STORE_W = 6,
  OP_ADD = 8,
  OP_MUL = 9,
  OP_DIV = 10,
  OP_NOR = 11,
  OP_MOVI = 16,
  OP_CMOV = 18,
  OP_IN = 24,
  OP_OUT = 25,
  OP_READ = 26,
  OP_WRITE = 27,
  OP_HALT = 31,
};

typedef uint8_t byte;
typedef uint32_t word;

/* Machine state */
static int disk;
static byte mem[MEM_SIZE];
static word regs[64] = {0};
#define MEM(addr) (*(assert((addr) < MEM_SIZE), &mem[(addr)]))

static void printBits2(unsigned int n, int bitsCount) {
  for (int i = bitsCount - 1; i >= 0; i--) {
    std::cout << ((n >> i) & 1);
  }
  printf("\n");
}

static unsigned int opCode(word inst) { return (inst >> 27) & 0b11111; }

static unsigned int
aReg(word inst) { // a-register - skip first 5 bits and get the next 6 bits
  return (inst >> 21) & 0b111111; // 32-5-6 = 21
}
static unsigned int
bReg(word inst) { // b-register - skip first 11 bits and get the next 6 bits
  return (inst >> 15) & 0b111111; // 32-5-6-6 = 15
}
static unsigned int
cReg(word inst) { // c-register - skip first 11 bits and get the next 6 bits
  return (inst >> 9) & 0b111111; // 32-5-6-6-6 = 9
}
static unsigned int getLowerByte(word inst) { return inst & 0b11111111; }
static word getLowerHalf(word inst) { return inst & 0xffff; }

/* original code */
static void disk_seek(word sector) {
  if ((off_t)-1 == lseek(disk, sector * 0x200, SEEK_SET)) {
    perror("lseek");
    exit(EXIT_FAILURE);
  }
}

static void disk_read(word addr, word sector) {
  ssize_t ret;
  assert(addr % SECTOR_SIZE == 0 && "alignment error (read)");

  // printf("");

  disk_seek(sector);
  ret = read(disk, &MEM(addr), SECTOR_SIZE);
  switch (ret) {

  case SECTOR_SIZE:
    break;

  case -1:
    perror("read");
    exit(EXIT_FAILURE);

  default:
    fprintf(stderr, "disk read error");
    exit(EXIT_FAILURE);
  }
}

static void disk_write(word addr, word sector) {
  ssize_t ret;
  assert(addr % SECTOR_SIZE == 0 && "alignment error (write)");
  disk_seek(sector);
  ret = write(disk, &MEM(addr), SECTOR_SIZE);

  switch (ret) {
  case SECTOR_SIZE:
    break;

  case -1:
    perror("write");
    exit(EXIT_FAILURE);

  default:
    fprintf(stderr, "disk write error");
    exit(EXIT_FAILURE);
  }
}

static bool spin() {
  word inst, pc;
  unsigned int op, r, i, s;

  pc = regs[REG_PC];
  assert(pc % 4 == 0 && "PC unaligned");

  /* Read 32bit big endian word */
  inst = MEM(pc) << 24 | MEM(pc + 1) << 16 | MEM(pc + 2) << 8 | MEM(pc + 3);

  regs[REG_PC] += 4; /* Increment PC before instruction execution */
  regs[REG_Z] = 0;   /* Register 0 must always be zero */
  op = inst >> 27;   /* Get the operation code */

  /* Execute MOVI-type instruction */
  if (OP_MOVI == op) {
    r = (inst >> 21) & 077;
    i = (inst >> 5) & 0xffff;
    s = (inst)&037;
    regs[r] = i << s;

    /* All other instructions follow the same format */
  } else {

    // get 3 x 6 bit parts
    unsigned int a = aReg(inst);
    unsigned int b = bReg(inst);
    unsigned int c = cReg(inst);

    word bcAddress = regs[b] + regs[c];

    // printf ("pc=%d op=%d a=%d b=%d c=%d\n", pc, op, a,b,c);

    if (OP_LOAD_B ==
        op) { // =  0 copy the byte at address (B) and (C) to register (A)
      regs[a] = (word)mem[bcAddress];
      return true;
    }

    if (OP_LOAD_H ==
        op) { // =  1 copy the half word at address (B) and (C) to register (A)
      regs[a] = ((word)mem[bcAddress] << 8) + mem[bcAddress + 1];
      return true;
    }

    if (OP_LOAD_W ==
        op) { // =  2 copy the word at address (B) and (C) to register (A)
      regs[a] = (word)((mem[bcAddress] << 24) + (mem[bcAddress + 1] << 16) +
                       (mem[bcAddress + 2] << 8) + mem[bcAddress + 3]);
      return true;
    }

    if (OP_STORE_B ==
        op) { // #4 store the lower byte of (A) at address (B) + (C)
      mem[bcAddress] = getLowerByte(regs[a]);
      return true;
    }

    if (OP_STORE_H ==
        op) { // #5 store the lower half word  (A) at address (B) + (C)
      word lHalf = getLowerHalf(regs[a]);
      mem[bcAddress] = (lHalf >> 8) & 0xff;
      mem[bcAddress + 1] = lHalf & 0xff;
      return true;
    }

    if (OP_STORE_W == op) { // #6 store A at address (B) + (C)
      word w = regs[a];
      mem[bcAddress] = (w >> 24) & 0xff;
      mem[bcAddress + 1] = (w >> 16) & 0xff;
      mem[bcAddress + 2] = (w >> 8) & 0xff;
      mem[bcAddress + 3] = (w)&0xff;
      return true;
    }

    if (OP_ADD == op) { // 8 = set register A to (B) + (C)
      regs[a] = regs[b] + regs[c];
      return true;
    }

    if (OP_MUL == op) { // 9 = set register A to (B) * (C)
      regs[a] = regs[b] * regs[c];
      return true;
    }

    if (OP_DIV ==
        op) { // 10 = set register A to (B) / (C) (undefined if reg(C)==0)
      if (regs[c] != 0) {
        regs[a] = regs[b] / regs[c];
      }
      return true;
    }

    if (OP_NOR == op) { // 11 register A to  ~ (B) | (C)
      regs[a] = ~(regs[b] | regs[c]);
      return true;
    }

    if (OP_CMOV == op) { // = 18: set register A to B (except undefined if c==0)
      if (regs[c] != 0) {
        regs[a] = regs[b];
      }
      return true;
    }

    if (OP_IN == op) { // 24 read one char from console and register A to its
                       // value according ASCII table
      char input = getchar();
      regs[a] = int(input);
      return true;
    }

    if (OP_OUT == op) { // 25 writehe the character in the ASCII table given by
                        // the lower byt of (A)  to the console
      byte asciikey = getLowerByte(regs[a]);
      std::cout << asciikey;
      return true;
    }

    if (OP_READ == op) { // #26 copy store sector (B) to volatile memory at
                         // address (A) - the destination must be aligned to 512
                         // bytes
      disk_read(regs[a], regs[b]);
      return true;
    }

    if (OP_WRITE ==
        op) { // 27 copy store sector (B) to volatile memory at address (A)
      disk_write(regs[a], regs[b]);
      return true;
    }

    if (OP_HALT == op) { // 31
      fprintf(stderr, "HALTed\n");
      return false;
    }

    /* TODO: ... */
    printf("ups. implementation for opcode %d is missing\n", op);
    return false;
  }

  /* Keep running */
  return true;
}

int main(int argc, char *argv[]) {

  printf("\nLoading. Please wait ... \n\n");
  if (2 != argc) {
    fprintf(stderr, "usage: %s <disk>\n", argv[0]);
    return EXIT_FAILURE;
  }

  disk = open(argv[1], O_RDWR);
  if (-1 == disk) {
    perror("open");
    return EXIT_FAILURE;
  }

  disk_read(0, 0);
  while (spin())
    ;

  close(disk);

  return EXIT_SUCCESS;
}
